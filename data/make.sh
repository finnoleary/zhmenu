#!/bin/bash

# cedict="cedict_1_0_ts_utf-8_mdbg.txt"
# freq="sorted.utf8.txt"
# output="sc-pinyin.txt"
# 
# Frequency format data:
# <simplified chinese>\t<frequency>\t<radical number>\n
# input="$(cat $freq | awk '{print $1}' -)"
# 
# awk '{print $1}' $freq | while read -r line; do
# 	# cedict format:
# 	# <simplified chinese>\t<traditional chinese>\t[<pinyin>]\t\/<definitions>\/<etc.>\/\n
# 	result=$(grep '^'$line' ' $cedict                       \
# 	       | head -n 1                                      \
# 	       | sed -E 's/.* .* \[(.*)\] \/(.*)$/\1\t\2/;' -   \
# 	       )
# 
# 	# Resulting file is of form:
# 	# <simplified chinese>\t<pinyin>\t<definition>\n
# 	printf "%s\t$result\n" "$line" | grep '[a-zA-Z]' - >> $output
# done

function strip {
	sed 'y/āáǎàēéěèīíǐìōóǒòūúǔùǖǘǚǜĀÁǍÀĒÉĚÈĪÍǏÌŌÓǑÒŪÚǓÙǕǗǙǛ/aaaaeeeeiiiioooouuuuüüüüAAAAEEEEIIIIOOOOUUUUÜÜÜÜ/' -
}

reading_file="Unihan_Readings.txt" # kMandarin and kDefinition (Also maybe kCantonese if we want that in the future)
diction_file="Unihan_DictionaryLikeData.txt" # kFrequency
tmpfile="sc-pinyin.txt.tmp"
outfile="$1"

# codepoint_data=$(  grep -v "^#" $reading_file | awk '{print $1}' - | uniq )
mandarin_data=$(   grep 'kMandarin'   $reading_file | grep -v "^#" -)
definition_data=$( grep 'kDefinition' $reading_file | grep -v "^#" -)
frequency_data=$(  grep 'kFrequency'  $diction_file | grep -v "^#" -)

# This is not the most efficient way to do this (:
while read -r line; do
	codepoint=$(awk '{print $1}' <<< $line)
	mandarin=$( awk '{print $3}' <<< $line | strip )
	frequency=$( grep "^$codepoint" <<< $frequency_data | awk '{print $3}' - )
	definition=$(grep "^$codepoint" <<< $definition_data | sed -e 's/.*kDefinition\s*//' -)

	code=$( echo "$codepoint" | sed 's/U+//' -)
	char=$( ./utf8 "$code" )
	freq=$( [[ -z "$frequency" ]] && echo 6 || echo $frequency )
	def=$(  [[ -z "$definition" ]] && echo "" || echo $definition )

	printf "%s\t%s\t%s\t'%s'\n" "$char" "$mandarin" "$freq" "$def"
done <<< $mandarin_data > $tmpfile

sort -k3 -n $tmpfile > $outfile
rm $tmpfile
