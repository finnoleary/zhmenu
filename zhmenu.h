
/* zhm_Display functions */
int     win_open       (zhm_Display *window, int x, int y, size_t width, ssize_t height, char *schemetable[SCM_NUMITEMS],
                        int force_position, char *font);
void    win_close      (zhm_Display *window);
int     win_drawrect   (zhm_Display *window, XftColor *color, int x, int y, uint w, uint h);
int     win_drawstring (zhm_Display *window, XftColor *color, char *s, size_t l, int x, int y);
ssize_t win_drawarrow  (zhm_Display *window, zhm_Input *input, XftColor *fg, XftColor *bg, size_t x, char *arrow);
ssize_t win_drawitem   (zhm_Display *window, zhm_Input *input, XftColor *fg, XftColor *bg, int selected, size_t x,
                        struct entry *e);
int     win_blit       (zhm_Display *window);
int     win_clear      (zhm_Display *window);
int     win_raise      (zhm_Display *window);
int     win_resize     (zhm_Display *window, int width, int height);

/* zhm_ColorScheme functions */
int     col_load       (zhm_Display *window, zhm_ColorScheme *scheme, char *schemetable[SCM_NUMITEMS]);
void    col_unload     (zhm_Display *window, zhm_ColorScheme *scheme);

/* zhm_Table functions and associated */
size_t  file_lines      (char *buffer);
char *  file_buffer     (char *path);
char *  table_path      (char *paths);
int     table_open      (zhm_Table *t, char *paths);
void    table_close     (zhm_Table *t);

/* zhm_Input functions */
int     key_grab        (zhm_Display *window);
void    key_ungrab      (zhm_Display *window);
int     key_open        (zhm_Display *window, zhm_Input *input, zhm_Table *table);
void    key_close       (zhm_Input *input);
int     key_addgrapheme (zhm_Input *input, char grapheme[UTFmax]);
void    key_delete      (zhm_Input *input);
void    key_rubout      (zhm_Input *input);
void    key_prevchar    (zhm_Input *input);
void    key_nextchar    (zhm_Input *input);

/* match-related functions */
void            match_next        (zhm_Input *input);
struct entry *  match_poolindex   (zhm_Input *input, zhm_Table *table, size_t i);
void            match_prev        (zhm_Input *input);
void            match_gostart     (zhm_Input *input);
void            match_select      (zhm_Input *input, zhm_Table *table);
void            match_populate    (zhm_Input *input, zhm_Table *table);
void            match_output      (zhm_Input *input, zhm_Table *table);
void            match_draw        (zhm_Display *window, zhm_Input *input, zhm_Table *table);
int             pinyincmp         (char *pinyin, char *string);

/* main loop */
int             win_drawstate     (zhm_Display *window, zhm_Input *input, zhm_Table *table);
int             key_press         (zhm_Input *input, zhm_Table *table, XKeyEvent *keyevent);

/* misc functions */
char *          get_error_string  (Display *display, int error_code);
const char *    ettoa             (int event_type);
ssize_t         atolu             (char *s);
void            help              (char *progname, size_t width, size_t x, size_t y);
int             parse_arguments   (char **argv, size_t *width, size_t *x, size_t *y, char **fontstr, char **dbpath);
int             main              (int argc, char **argv);

